const websocket = require("ws");

const wss = new websocket.Server({port: 5983});
const express = require('express');
const config = require('./config.json');
const app = express();


const servers = new Map();
let recvMap = {
    4: "INIT_CON",
    1: "PING_BACK",
    3: "DOCKER_STATUS",
    5: "PING"
};

wss.on('connection', function con(ws){
    console.log("[Docker Manager] New server connected, trying to get all docker processes.");

    //S is the status code 4 meaning init
    ws.send(JSON.stringify({s: "4", d: {
        token_required: false,
        token: null
    }}));

    ws.on('message', function incoming(data) {
        data = JSON.parse(data);
        if(data.s == 5){
			ws.server_name = data.d.server_name;
			servers.set(ws.server_name, {latency: null, status: null, docker_containers: []});
            sendUpdateRequests(ws);
		}

    	if(data.s == 6) {
        	console.log(`Docker Manager] Latency for server ${ws.server_name} is ${data.d.latency}ms`);
        	ws.send(JSON.stringify({s: "8"}));
    	}

		if(data.s == 8){
			if(data.d.containers !== null){
				let server = servers.get(ws.server_name);
				servers.set(ws.server_name, {latency: server.latency, status: server.status, docker_containers: data.d.containers});
			} else {
				let server = servers.get(ws.server_name);
				servers.set(ws.server_name, {latency: server.latency, status: server.status, docker_containers: []});
			}

		}

		if(data.s == 1){
			if(!data.d){
				ws.send(JSON.stringify({s: "10"}));
			}else {
				console.log(`Docker Manager] Latency for server ${ws.server_name} is ${data.d.latency}ms`);
				let server = servers.get(ws.server_name);
				servers.set(ws.server_name, {latency: data.d.latency, status: server.status, docker_containers: server.docker_containers});
			}
		}
    });
});



function sendUpdateRequests(ws){
	setTimeout(() => { 
		ws.send(JSON.stringify({s: "1"}));
		ws.send(JSON.stringify({s: "8"}));
		sendUpdateRequests(ws);
	}, config.requestIntervals);
}

app.set("servers", servers);


app.get("/:server/containers", (req, res, next) => {
	let server = servers.get(req.params.server);
	if(!server){
		res.json({error: "Could not find server"}).status(404);
	}

	return res.json({server: server});
});

app.listen(2021, () => {
	console.log("Web server is listening on port 2021");
});