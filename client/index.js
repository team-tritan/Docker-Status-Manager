const WebSocket = require('ws');
const Docker = require('dockerode');
const ws = new WebSocket('ws://localhost:5983');
var os = require("os");
var docker = new Docker({socketPath: '/var/run/docker.sock'});

let recvMap = {
    4: "INIT_CON",
    1: "PING_BACK",
    3: "DOCKER_STATUS"
};

ws.on('open', function open() {
    console.log("Connected to host.");
});

let startTime;

ws.on('message', function incoming(data) {
    data = JSON.parse(data);
    if(data.s == 4){
        if(data.d.token_required == true){
            //authenticate
        }

        startTime = Date.now();
        
        ws.send(JSON.stringify({s: "5", d: {
            server_name: os.hostname()
        }}));
    }
    if(data.s == 6){
        ws.send(JSON.stringify({s: "6", d: {
            latency: Date.now() - startTime
        }}));
    }

    if(data.s == 8){
        docker.listContainers(function (err, containers) {
            ws.send(JSON.stringify({s: "8", d: {
                containers: containers
            }}));
        });
    }

    if(data.s == 1){
        startTime = Date.now();
        ws.send(JSON.stringify({s: "1"}));
    }

    if(data.s == 10){
        ws.send(JSON.stringify({s: "1", d: {
            latency: Date.now() - startTime
        }}));
    }



});
